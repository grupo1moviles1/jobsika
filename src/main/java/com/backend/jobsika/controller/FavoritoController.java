package com.backend.jobsika.controller;

import com.backend.jobsika.exception.CustomException;
import com.backend.jobsika.service.FavoritoService;
import com.backend.jobsika.service.PuestoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/v1/favorito")
public class FavoritoController {

    @Autowired
    private FavoritoService favoritoService;

    @GetMapping("/usuario/{idusuario}")
    private ResponseEntity<?> findAllFavoritos(@PathVariable String idusuario){
        return ResponseEntity.ok(favoritoService.findAllFavoritos(idusuario));
    }

    @PostMapping("/registrar/{idusuario}/{idPuesto}")
    private ResponseEntity<?> findAllFavoritos(@PathVariable String idusuario, @PathVariable String idPuesto) throws CustomException {
        favoritoService.registrar(UUID.fromString(idusuario), UUID.fromString(idPuesto));
        return ResponseEntity.ok("{\"response\":\"ok\"}");
    }

    @PutMapping("/eliminar/{idusuario}/{idPuesto}")
    private ResponseEntity<?> eliminar(@PathVariable String idusuario, @PathVariable String idPuesto) throws CustomException {
        favoritoService.eliminar(UUID.fromString(idusuario), UUID.fromString(idPuesto));
        return ResponseEntity.ok("{\"response\":\"ok\"}");
    }
}
