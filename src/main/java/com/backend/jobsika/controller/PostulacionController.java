package com.backend.jobsika.controller;

import com.backend.jobsika.exception.CustomException;
import com.backend.jobsika.service.PostulacionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/v1/postulacion")
public class PostulacionController {

    @Autowired
    private PostulacionService postulacionService;

    @PostMapping("/postulando/{idUsuario}/{idEmpresa}/{idPuesto}")
    private ResponseEntity<?> registrarPostulacion(
            @PathVariable String idUsuario,
            @PathVariable String idEmpresa,
            @PathVariable String idPuesto ) throws CustomException {

        postulacionService.registrarPostulacion(UUID.fromString(idUsuario), UUID.fromString(idEmpresa), UUID.fromString(idPuesto));
        return ResponseEntity.ok("{\"response\":\"ok\"}");
    }

    @PutMapping("/eliminar/{idUsuario}/{idEmpresa}/{idPuesto}")
    private ResponseEntity<?> eliminarPostulacion(
            @PathVariable String idUsuario,
            @PathVariable String idEmpresa,
            @PathVariable String idPuesto ) throws CustomException {

        postulacionService.eliminarPostulacion(UUID.fromString(idUsuario), UUID.fromString(idEmpresa), UUID.fromString(idPuesto));
        return ResponseEntity.ok("{\"response\":\"ok\"}");
    }

    @GetMapping("/mispostulaciones/{idUsuario}")
    private ResponseEntity<?> registrarPostulacion(
            @PathVariable String idUsuario){

        return ResponseEntity.ok(postulacionService.findPostulacionByIdUsuario(UUID.fromString(idUsuario)));
    }
}
