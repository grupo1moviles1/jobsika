package com.backend.jobsika.controller;

import com.backend.jobsika.dto.AuthenticationRequest;
import com.backend.jobsika.entity.Usuario;
import com.backend.jobsika.exception.CustomException;
import com.backend.jobsika.service.AuthenticationService;
import com.backend.jobsika.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/auth")
public class AuthenticationController {

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private AuthenticationService authenticationService;

    @PostMapping
    public ResponseEntity<?> login(@RequestBody AuthenticationRequest requestAuth) throws Exception {
        Map<String, Object> map = new HashMap<>();
        map.put("token",authenticationService.authenticate(requestAuth.getEmail(), requestAuth.getPassword()));
        return ResponseEntity.ok(map);
    }

    @GetMapping("/recuperar/{correo}")
    public ResponseEntity<?> recuperarClaveGenerarCodigoValidacion(@PathVariable String correo) throws Exception {

        authenticationService.recuperarClaveGenerarCodigoValidacion(correo);

        return ResponseEntity.ok("{\"response\":\"ok\"}");

    }

    @GetMapping("/validarcodigo/{codigoseguridad}/{correo}")
    public ResponseEntity<?> validarCodigo(@PathVariable String codigoseguridad, @PathVariable String correo) throws Exception {
        usuarioService.validarCodigoSeguridad(codigoseguridad, correo);
        return ResponseEntity.ok("{\"response\":\"ok\"}");
    }

    @PostMapping("/nuevaclave")
    public ResponseEntity<?> nuevaClave(@RequestBody AuthenticationRequest requestAuth) throws Exception {
        Map<String, Object> map = new HashMap<>();
        map.put("token",authenticationService.authenticateNuevaClave(requestAuth.getEmail(), requestAuth.getPassword()));
        return ResponseEntity.ok(map);
    }

}
