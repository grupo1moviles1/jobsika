package com.backend.jobsika.controller;

import com.backend.jobsika.service.PuestoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/v1/puesto")
public class PuestosController {

    @Autowired
    private PuestoService puestoService;

    @GetMapping("/{idUsuario}")
    private ResponseEntity<?> findAllPuestoEmpresa(@PathVariable String idUsuario){
        return ResponseEntity.ok(puestoService.findAllPuestoEmpresa(UUID.fromString(idUsuario)));
    }

    @GetMapping("/busqueda/{idUsuario}")
    private ResponseEntity<?> findAllPuestoEmpresa(@PathVariable String idUsuario, @RequestParam String valorBusqueda){
        return ResponseEntity.ok(puestoService.findAllPuestoEmpresaBusqueda(UUID.fromString(idUsuario), valorBusqueda));
    }

    @GetMapping("/ubicaciones")
    private ResponseEntity<?> findAllubicaciones(){
        return ResponseEntity.ok(puestoService.findAllubicaciones());
    }
}
