package com.backend.jobsika.controller;

import com.backend.jobsika.dto.UsuarioDto;
import com.backend.jobsika.entity.Usuario;
import com.backend.jobsika.exception.CustomException;
import com.backend.jobsika.service.AuthenticationService;
import com.backend.jobsika.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/usuario")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private AuthenticationService authenticationService;

    @PostMapping("/registrar")
    public ResponseEntity<?> registrarUsuario(@RequestBody UsuarioDto usuarioDto) throws Exception {

        System.out.println(usuarioDto);
        String pass = usuarioDto.getClave();
        Usuario us = usuarioService.registrarUsuario(usuarioDto);
        if(us==null){
            throw new CustomException(HttpStatus.PRECONDITION_FAILED,"Ocurrio un error al registrar");
        }

        Map<String, Object> map = new HashMap<>();
        map.put("token",authenticationService.authenticate(usuarioDto.getCorreo(), pass));
        return ResponseEntity.ok(map);
        //return ResponseEntity.ok("TEST");
    }

    @PutMapping("/actualizar")
    public ResponseEntity<?> actualizarUsuario(@RequestBody UsuarioDto usuarioDto) throws Exception {
        Usuario us = usuarioService.actualizarUsuario(usuarioDto);
        return ResponseEntity.ok("{\"response\":\"ok\"}");
    }

    @GetMapping("/buscarporusuario/{correo}")
    public ResponseEntity<?> registrarUsuario(@PathVariable String correo) throws Exception {

        Optional<Usuario> usuario= usuarioService.findByCorreo(correo);
        usuario.get().setClave(null);
        return ResponseEntity.ok(usuario.get());

    }
}
