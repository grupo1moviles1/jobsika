package com.backend.jobsika.dto;

import java.math.BigDecimal;
import java.util.UUID;

public class PuestoEmpresaDto {

    private UUID idPuesto;
    private UUID idEmpresa;
    private String nombrePuesto;
    private String descripcionPuesto;
    private String requisitosPuesto;
    private BigDecimal sueldo;
    private String nombreEmpresa;
    private String logoEmpresa;
    private String longitud;
    private String latitud;

    private boolean favorito;
    private boolean postulado;

    public UUID getIdPuesto() {
        return idPuesto;
    }

    public void setIdPuesto(UUID idPuesto) {
        this.idPuesto = idPuesto;
    }

    public UUID getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(UUID idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getNombrePuesto() {
        return nombrePuesto;
    }

    public void setNombrePuesto(String nombrePuesto) {
        this.nombrePuesto = nombrePuesto;
    }

    public String getDescripcionPuesto() {
        return descripcionPuesto;
    }

    public void setDescripcionPuesto(String descripcionPuesto) {
        this.descripcionPuesto = descripcionPuesto;
    }

    public String getRequisitosPuesto() {
        return requisitosPuesto;
    }

    public void setRequisitosPuesto(String requisitosPuesto) {
        this.requisitosPuesto = requisitosPuesto;
    }

    public BigDecimal getSueldo() {
        return sueldo;
    }

    public void setSueldo(BigDecimal sueldo) {
        this.sueldo = sueldo;
    }

    public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }

    public String getLogoEmpresa() {
        return logoEmpresa;
    }

    public void setLogoEmpresa(String logoEmpresa) {
        this.logoEmpresa = logoEmpresa;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public boolean getFavorito() {
        return favorito;
    }

    public void setFavorito(boolean favorito) {
        this.favorito = favorito;
    }

    public boolean getPostulado() {
        return postulado;
    }

    public void setPostulado(boolean postulado) {
        this.postulado = postulado;
    }

    @Override
    public String toString() {
        return "PuestoEmpresaDto{" +
                "idPuesto=" + idPuesto +
                ", idEmpresa=" + idEmpresa +
                ", nombrePuesto='" + nombrePuesto + '\'' +
                ", descripcionPuesto='" + descripcionPuesto + '\'' +
                ", requisitosPuesto='" + requisitosPuesto + '\'' +
                ", sueldo=" + sueldo +
                ", nombreEmpresa='" + nombreEmpresa + '\'' +
                ", logoEmpresa='" + logoEmpresa + '\'' +
                ", longitud='" + longitud + '\'' +
                ", latitud='" + latitud + '\'' +
                ", favorito=" + favorito +
                ", postulado=" + postulado +
                '}';
    }
}
