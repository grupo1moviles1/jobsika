package com.backend.jobsika.dto;

public class CoordenadasDto {
    private String latitud;
    private String longitud;

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public CoordenadasDto(String latitud, String longitud) {
        this.latitud = latitud;
        this.longitud = longitud;
    }


    public CoordenadasDto() {
    }

    @Override
    public String toString() {
        return "CoordenadasDto{" +
                "latitud='" + latitud + '\'' +
                ", longitud='" + longitud + '\'' +
                '}';
    }
}
