package com.backend.jobsika.util;

public class Constantes {
    public static final String NOMBRE_DEFAULT_ROL = "ROLE_USER";
    public static final String NOMBRE_DEFAULT_ROL_ADMIN = "ROLE_ADMIN";

    public static final String ESTADO_REGISTRO_ACTIVO = "1";
    public static final String ESTADO_SIN_EMPRESA = "0";
}
