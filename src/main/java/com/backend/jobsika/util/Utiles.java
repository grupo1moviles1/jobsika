package com.backend.jobsika.util;

import com.backend.jobsika.dto.UsuarioDto;
import com.backend.jobsika.entity.Usuario;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;

public class Utiles {

    public static ObjectMapper mapper = new ObjectMapper();

    public static Usuario usuarioDtoToUsuario(UsuarioDto usuarioDto){

        return mapper.convertValue(usuarioDto, Usuario.class);
    }
}
