package com.backend.jobsika.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.UUID;

@Embeddable
public class FavoritoPK implements Serializable {
    private static final long serialVersionUID = 3L;

    @Column(name = "id_usuario")
    private UUID idUsuario;

    @Column(name = "id_puesto")
    private UUID idPuesto;

    public UUID getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(UUID idUsuario) {
        this.idUsuario = idUsuario;
    }

    public UUID getIdPuesto() {
        return idPuesto;
    }

    public void setIdPuesto(UUID idPuesto) {
        this.idPuesto = idPuesto;
    }

    @Override
    public String toString() {
        return "FavoritoPK{" +
                "idUsuario=" + idUsuario +
                ", idPuesto=" + idPuesto +
                '}';
    }
}
