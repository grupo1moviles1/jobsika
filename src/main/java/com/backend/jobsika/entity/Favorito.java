package com.backend.jobsika.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "favoritos")
public class Favorito {

    @EmbeddedId
    private FavoritoPK favoritoPK;

    @Column(name = "fecha_creacion")
    private Date fechaCreacion;

    @Column(name = "usuario_creado")
    private UUID usuarioCreado;

    @Column(name = "fecha_modificado")
    private Date fechaModificado;

    @Column(name = "usuario_modificado")
    private UUID usuarioModificado;

    @Column(name = "estado")
    private String estado;

    public FavoritoPK getFavoritoPK() {
        return favoritoPK;
    }

    public void setFavoritoPK(FavoritoPK favoritoPK) {
        this.favoritoPK = favoritoPK;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public UUID getUsuarioCreado() {
        return usuarioCreado;
    }

    public void setUsuarioCreado(UUID usuarioCreado) {
        this.usuarioCreado = usuarioCreado;
    }

    public Date getFechaModificado() {
        return fechaModificado;
    }

    public void setFechaModificado(Date fechaModificado) {
        this.fechaModificado = fechaModificado;
    }

    public UUID getUsuarioModificado() {
        return usuarioModificado;
    }

    public void setUsuarioModificado(UUID usuarioModificado) {
        this.usuarioModificado = usuarioModificado;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return "Favorito{" +
                "favoritoPK=" + favoritoPK +
                ", fechaCreacion=" + fechaCreacion +
                ", usuarioCreado=" + usuarioCreado +
                ", fechaModificado=" + fechaModificado +
                ", usuarioModificado=" + usuarioModificado +
                ", estado='" + estado + '\'' +
                '}';
    }
}
