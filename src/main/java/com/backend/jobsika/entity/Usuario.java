package com.backend.jobsika.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "usuario")
public class Usuario {

    @Id
    @Column(name = "id_usuario")
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID idUsuario;

    @Column(name = "nombre_usuario")
    private String nombreUsuario;

    @Column(name = "apellido_usuario")
    private String apellidoUsuario;

    @Column(name = "dni")
    private String dni;

    @Column(name = "telefono")
    private String telefono;

    @Column(name = "fecha_nacimiento")
    private Date fechaNacimiento;

    @Column(name = "sexo")
    private String sexo;

    @Column(name = "clave")
    private String clave;

    @Column(name = "correo")
    private String correo;

    @Column(name = "id_empresa")
    private UUID idEmpresa;

    @Column(name = "indicador_empresa")
    private String indicadorEmpresa;

    @Column(name = "foto")
    private String foto;

    @Column(name = "codigo_recuperar")
    private String codigoRecuperar;

    @Column(name = "fecha_creacion")
    private Date fechaCreacion;

    @Column(name = "usuario_creado")
    private UUID usuarioCreado;

    @Column(name = "fecha_modificado")
    private Date fechaModificado;

    @Column(name = "usuario_modificado")
    private UUID usuarioModificado;

    @Column(name = "estado")
    private String estado;

    /*@OneToMany(fetch = FetchType.LAZY, mappedBy = "detalleUsuarioRolPK.idUsuario")
    private List<DetalleUsuarioRol> detallesUsuarioRol;*/

    public UUID getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(UUID idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getApellidoUsuario() {
        return apellidoUsuario;
    }

    public void setApellidoUsuario(String apellidoUsuario) {
        this.apellidoUsuario = apellidoUsuario;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public UUID getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(UUID idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getIndicadorEmpresa() {
        return indicadorEmpresa;
    }

    public void setIndicadorEmpresa(String indicadorEmpresa) {
        this.indicadorEmpresa = indicadorEmpresa;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getCodigoRecuperar() {
        return codigoRecuperar;
    }

    public void setCodigoRecuperar(String codigoRecuperar) {
        this.codigoRecuperar = codigoRecuperar;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }


    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public UUID getUsuarioCreado() {
        return usuarioCreado;
    }

    public void setUsuarioCreado(UUID usuarioCreado) {
        this.usuarioCreado = usuarioCreado;
    }

    public Date getFechaModificado() {
        return fechaModificado;
    }

    public void setFechaModificado(Date fechaModificado) {
        this.fechaModificado = fechaModificado;
    }

    public UUID getUsuarioModificado() {
        return usuarioModificado;
    }

    public void setUsuarioModificado(UUID usuarioModificado) {
        this.usuarioModificado = usuarioModificado;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    /*public List<DetalleUsuarioRol> getDetallesUsuarioRol() {
        return detallesUsuarioRol;
    }

    public void setDetallesUsuarioRol(List<DetalleUsuarioRol> detallesUsuarioRol) {
        this.detallesUsuarioRol = detallesUsuarioRol;
    }*/

    @Override
    public String toString() {
        return "Usuario{" +
                "idUsuario=" + idUsuario +
                ", nombreUsuario='" + nombreUsuario + '\'' +
                ", apellidoUsuario='" + apellidoUsuario + '\'' +
                ", dni='" + dni + '\'' +
                ", telefono='" + telefono + '\'' +
                ", fechaNacimiento=" + fechaNacimiento +
                ", sexo='" + sexo + '\'' +
                ", clave='" + clave + '\'' +
                ", correo='" + correo + '\'' +
                ", idEmpresa=" + idEmpresa +
                ", indicadorEmpresa='" + indicadorEmpresa + '\'' +
                ", foto='" + foto + '\'' +
                ", codigo_recuperar='" + codigoRecuperar + '\'' +
                ", fecha_creacion=" + fechaCreacion +
                ", usuarioCreado=" + usuarioCreado +
                ", fechaModificado=" + fechaModificado +
                ", usuarioModificado=" + usuarioModificado +
                ", estado='" + estado + '\'' +
                '}';
    }
}
