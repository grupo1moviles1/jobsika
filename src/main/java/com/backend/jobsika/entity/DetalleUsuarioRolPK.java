package com.backend.jobsika.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.UUID;

@Embeddable
public class DetalleUsuarioRolPK implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "id_usuario")
    private UUID idUsuario;

    @Column(name = "id_rol")
    private UUID idRol;

    public UUID getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(UUID idUsuario) {
        this.idUsuario = idUsuario;
    }

    public UUID getIdRol() {
        return idRol;
    }

    public void setIdRol(UUID idRol) {
        this.idRol = idRol;
    }

    @Override
    public String toString() {
        return "DetalleUsuarioRolPK{" +
                "idUsuario=" + idUsuario +
                ", idRol=" + idRol +
                '}';
    }
}
