package com.backend.jobsika.entity;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "detalle_usuario_rol")
public class DetalleUsuarioRol {

    @EmbeddedId
    private DetalleUsuarioRolPK detalleUsuarioRolPK;

    @Column(name = "fecha_creacion")
    private Date fechaCreacion;

    @Column(name = "usuario_creado")
    private UUID usuarioCreado;

    @Column(name = "fecha_modificado")
    private Date fechaModificado;

    @Column(name = "usuario_modificado")
    private UUID usuarioModificado;

    @Column(name = "estado")
    private String estado;

    /*@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_usuario", referencedColumnName = "id_usuario", insertable = false , updatable = false)
    private Usuario usuario;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_rol", referencedColumnName = "id_rol", insertable = false , updatable = false)
    private Rol rol;*/

    public DetalleUsuarioRolPK getDetalleUsuarioRolPK() {
        return detalleUsuarioRolPK;
    }

    public void setDetalleUsuarioRolPK(DetalleUsuarioRolPK detalleUsuarioRolPK) {
        this.detalleUsuarioRolPK = detalleUsuarioRolPK;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public UUID getUsuarioCreado() {
        return usuarioCreado;
    }

    public void setUsuarioCreado(UUID usuarioCreado) {
        this.usuarioCreado = usuarioCreado;
    }

    public Date getFechaModificado() {
        return fechaModificado;
    }

    public void setFechaModificado(Date fechaModificado) {
        this.fechaModificado = fechaModificado;
    }

    public UUID getUsuarioModificado() {
        return usuarioModificado;
    }

    public void setUsuarioModificado(UUID usuarioModificado) {
        this.usuarioModificado = usuarioModificado;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    /*public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Rol getRol() {
        return rol;
    }

    public void setRol(Rol rol) {
        this.rol = rol;
    }*/

    @Override
    public String toString() {
        return "DetalleUsuarioRol{" +
                "detalleUsuarioRolPK=" + detalleUsuarioRolPK +
                ", fecha_creacion=" + fechaCreacion +
                ", usuarioCreado=" + usuarioCreado +
                ", fechaModificado=" + fechaModificado +
                ", usuarioModificado=" + usuarioModificado +
                ", estado='" + estado + '\'' +
                //", usuario=" + usuario +
                //", rol=" + rol +
                '}';
    }
}
