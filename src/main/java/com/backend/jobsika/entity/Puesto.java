package com.backend.jobsika.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "puestos")
public class Puesto {

    @Id
    @Column(name = "id_puesto")
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID idPuesto;

    @Column(name = "id_empresa")
    private UUID idEmpresa;

    @Column(name = "nombre_puesto")
    private String nombrePuesto;

    @Column(name = "descripcion_puesto")
    private String descripcionPuesto;

    @Column(name = "requisitos_puesto")
    private String requisitosPuesto;

    @Column(name = "sueldo")
    private BigDecimal sueldo;



    @Column(name = "fecha_creacion")
    private Date fechaCreacion;

    @Column(name = "usuario_creado")
    private UUID usuarioCreado;

    @Column(name = "fecha_modificado")
    private Date fechaModificado;

    @Column(name = "usuario_modificado")
    private UUID usuarioModificado;

    @Column(name = "estado")
    private String estado;

    public UUID getIdPuesto() {
        return idPuesto;
    }

    public void setIdPuesto(UUID idPuesto) {
        this.idPuesto = idPuesto;
    }

    public UUID getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(UUID idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getNombrePuesto() {
        return nombrePuesto;
    }

    public void setNombrePuesto(String nombrePuesto) {
        this.nombrePuesto = nombrePuesto;
    }

    public String getDescripcionPuesto() {
        return descripcionPuesto;
    }

    public void setDescripcionPuesto(String descripcionPuesto) {
        this.descripcionPuesto = descripcionPuesto;
    }

    public String getRequisitosPuesto() {
        return requisitosPuesto;
    }

    public void setRequisitosPuesto(String requisitosPuesto) {
        this.requisitosPuesto = requisitosPuesto;
    }

    public BigDecimal getSueldo() {
        return sueldo;
    }

    public void setSueldo(BigDecimal sueldo) {
        this.sueldo = sueldo;
    }


    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public UUID getUsuarioCreado() {
        return usuarioCreado;
    }

    public void setUsuarioCreado(UUID usuarioCreado) {
        this.usuarioCreado = usuarioCreado;
    }

    public Date getFechaModificado() {
        return fechaModificado;
    }

    public void setFechaModificado(Date fechaModificado) {
        this.fechaModificado = fechaModificado;
    }

    public UUID getUsuarioModificado() {
        return usuarioModificado;
    }

    public void setUsuarioModificado(UUID usuarioModificado) {
        this.usuarioModificado = usuarioModificado;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return "Puesto{" +
                "idPuesto=" + idPuesto +
                ", idEmpresa=" + idEmpresa +
                ", nombrePuesto='" + nombrePuesto + '\'' +
                ", descripcionPuesto='" + descripcionPuesto + '\'' +
                ", requisitosPuesto='" + requisitosPuesto + '\'' +
                ", sueldo=" + sueldo +

                ", fechaCreacion=" + fechaCreacion +
                ", usuarioCreado=" + usuarioCreado +
                ", fechaModificado=" + fechaModificado +
                ", usuarioModificado=" + usuarioModificado +
                ", estado='" + estado + '\'' +
                '}';
    }
}
