package com.backend.jobsika.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.UUID;

@Embeddable
public class PostulacionPK implements Serializable {
    private static final long serialVersionUID = 2L;

    @Column(name = "id_puesto")
    private UUID idPuesto;

    @Column(name = "id_usuario")
    private UUID idUsuario;

    @Column(name = "id_empresa")
    private UUID idEmpresa;

    public UUID getIdPuesto() {
        return idPuesto;
    }

    public void setIdPuesto(UUID idPuesto) {
        this.idPuesto = idPuesto;
    }

    public UUID getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(UUID idUsuario) {
        this.idUsuario = idUsuario;
    }

    public UUID getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(UUID idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    @Override
    public String toString() {
        return "PostulacionPK{" +
                "idPuesto=" + idPuesto +
                ", idUsuario=" + idUsuario +
                ", idEmpresa=" + idEmpresa +
                '}';
    }
}
