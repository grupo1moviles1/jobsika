package com.backend.jobsika;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JobsikaApplication {

	public static void main(String[] args) {
		SpringApplication.run(JobsikaApplication.class, args);
	}

}
