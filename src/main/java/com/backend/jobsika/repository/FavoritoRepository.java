package com.backend.jobsika.repository;

import com.backend.jobsika.entity.Favorito;
import com.backend.jobsika.entity.FavoritoPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface FavoritoRepository extends JpaRepository<Favorito, FavoritoPK> {

    @Modifying
    @Query(value = "select \n" +
            "Cast(p.id_puesto as varchar) as idPuesto,\n" +
            "Cast(p.id_empresa as varchar)  as idEmpresa,\n" +
            "p.nombre_puesto as nombrePuesto,\n" +
            "p.descripcion_puesto as descripcionPuesto,\n" +
            "p.requisitos_puesto as requisitosPuesto,\n" +
            "p.sueldo as sueldo,\n" +
            "e.nombre_empresa as nombreEmpresa,\n" +
            "e.logo_empresa as logoEmpresa,\n" +
            "e.longitud as longitud,\n" +
            "e.latitud as latitud,\n" +
            "CASE WHEN f.estado = '1' THEN 1\n" +
            "\tELSE 0\n" +
            "END AS favorito,\n" +
            "\n" +
            "CASE WHEN p2.estado = '1' THEN 1\n" +
            "\tELSE 0\n" +
            "END AS postulacion\n" +
            "\n" +
            "from puestos p \n" +
            "inner join empresa e on p.id_empresa = e.id_empresa\n" +
            "inner  join favoritos f on f.id_puesto  = p.id_puesto\n" +
            "left join postulaciones p2 on p2.id_puesto =p.id_puesto and p2.id_usuario  = :idUsuarioPostulacion and p2.estado = '1'\n" +
            "where f.estado = '1' and f.id_usuario = :idUsuarioFavorito", nativeQuery = true)
    List<Object[]> findAllFavoritos( UUID idUsuarioPostulacion, UUID idUsuarioFavorito);

    @Query("select f from Favorito f where f.favoritoPK.idUsuario=:idUsuario and f.favoritoPK.idPuesto=:idPuesto")
    Favorito findFavorito(UUID idUsuario,UUID idPuesto);

}
