package com.backend.jobsika.repository;

import com.backend.jobsika.entity.Postulacion;
import com.backend.jobsika.entity.PostulacionPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface PostulacionRepository extends JpaRepository<Postulacion, PostulacionPK> {

    @Modifying
    @Query(value = "select \n" +
            "Cast(p.id_puesto as varchar) as idPuesto,\n" +
            "Cast(p.id_empresa as varchar)  as idEmpresa,\n" +
            "p.nombre_puesto as nombrePuesto,\n" +
            "p.descripcion_puesto as descripcionPuesto,\n" +
            "p.requisitos_puesto as requisitosPuesto,\n" +
            "p.sueldo as sueldo,\n" +
            "e.nombre_empresa as nombreEmpresa,\n" +
            "e.logo_empresa as logoEmpresa,\n" +
            "e.longitud as longitud,\n" +
            "e.latitud as latitud,\n" +
            "CASE WHEN f.estado = '1' THEN 1\n" +
            "\tELSE 0\n" +
            "END AS favorito,\n" +
            "CASE WHEN p2.estado = '1' THEN 1\n" +
            "\tELSE 0\n" +
            "END AS postulacion\n" +
            "\n" +
            "from puestos p \n" +
            "inner join empresa e on p.id_empresa = e.id_empresa\n" +
            "inner join postulaciones p2 on p2.id_puesto = p.id_puesto \n" +
            "left  join favoritos f on f.id_puesto  = p.id_puesto and f.estado = '1'\n" +
            "where p2.estado='1' and p2.id_usuario  = :idUsuario", nativeQuery = true)
    List<Object[]> findPostulacionByIdUsuario(UUID idUsuario);

    @Query("SELECT p from Postulacion p where  p.postulacionPK.idUsuario = :identificacionUsuario and p.postulacionPK.idEmpresa = :identificacionEmpresa and p.postulacionPK.idPuesto = :identificacionPuesto")
    Postulacion findPostulacion(UUID identificacionUsuario, UUID identificacionEmpresa, UUID identificacionPuesto);

}
