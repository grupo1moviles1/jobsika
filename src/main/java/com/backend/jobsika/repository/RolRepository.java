package com.backend.jobsika.repository;

import com.backend.jobsika.entity.Rol;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface RolRepository extends JpaRepository<Rol, UUID> {
    @Query("SELECT r FROM Rol r WHERE r.idRol IN ( :listUUID)")
    Optional<List<Rol>> findAllIdRol(List<UUID> listUUID);

    Optional<Rol> findByNombreRol(String nombreRol);
}
