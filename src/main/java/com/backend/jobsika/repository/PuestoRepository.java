package com.backend.jobsika.repository;

import com.backend.jobsika.dto.PuestoEmpresaDto;
import com.backend.jobsika.entity.Puesto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface PuestoRepository extends JpaRepository<Puesto, UUID> {

    @Modifying
    @Query(value = "select \n" +
            "Cast(p.id_puesto as varchar) as idPuesto,\n" +
            "Cast(p.id_empresa as varchar)  as idEmpresa,\n" +
            "p.nombre_puesto as nombrePuesto,\n" +
            "p.descripcion_puesto as descripcionPuesto,\n" +
            "p.requisitos_puesto as requisitosPuesto,\n" +
            "p.sueldo as sueldo,\n" +
            "e.nombre_empresa as nombreEmpresa,\n" +
            "e.logo_empresa as logoEmpresa,\n" +
            "e.longitud as longitud,\n" +
            "e.latitud as latitud,\n" +
            "CASE WHEN f.estado = '1' THEN 1\n" +
            "\tELSE 0\n" +
            "END AS favorito,\n" +
            "\n" +
            "CASE WHEN p2.estado = '1' THEN 1\n" +
            "\tELSE 0\n" +
            "END AS postulacion\n" +
            "\n" +
            "from puestos p \n" +
            "inner join empresa e on p.id_empresa = e.id_empresa\n" +
            "left  join favoritos f on f.id_puesto  = p.id_puesto and f.id_usuario = :idUsuarioFavorito and f.estado ='1'\n" +
            "left join postulaciones p2 on p2.id_puesto =p.id_puesto and p2.id_usuario  = :idUsuarioPostulacion and p2.estado = '1'", nativeQuery = true)
    List<Object[]> buscarPuestosEmpresas(UUID idUsuarioFavorito, UUID idUsuarioPostulacion);

    @Modifying
    @Query(value = "select \n" +
            "Cast(p.id_puesto as varchar) as idPuesto,\n" +
            "Cast(p.id_empresa as varchar)  as idEmpresa,\n" +
            "p.nombre_puesto as nombrePuesto,\n" +
            "p.descripcion_puesto as descripcionPuesto,\n" +
            "p.requisitos_puesto as requisitosPuesto,\n" +
            "p.sueldo as sueldo,\n" +
            "e.nombre_empresa as nombreEmpresa,\n" +
            "e.logo_empresa as logoEmpresa,\n" +
            "e.longitud as longitud,\n" +
            "e.latitud as latitud,\n" +
            "CASE WHEN f.estado = '1' THEN 1\n" +
            "\tELSE 0\n" +
            "END AS favorito,\n" +
            "CASE WHEN p2.estado = '1' THEN 1\n" +
            "\tELSE 0\n" +
            "END AS postulacion\n" +
            "from puestos p \n" +
            "inner join empresa e on p.id_empresa = e.id_empresa\n" +
            "left  join favoritos f on f.id_puesto  = p.id_puesto and f.id_usuario = :idUsuarioFavorito and f.estado ='1'\n" +
            "left join postulaciones p2 on p2.id_puesto =p.id_puesto and p2.id_usuario  = :idUsuarioPostulacion and p2.estado = '1'\n" +
            "where p.nombre_puesto like %:valorBusquedaNombre% or p.descripcion_puesto like %:valorBusquedaPuesto% or p.requisitos_puesto like %:valorBusquedaRequisito%", nativeQuery = true)
    List<Object[]> buscarPuestosEmpresasBusqueda(UUID idUsuarioFavorito, UUID idUsuarioPostulacion, String valorBusquedaNombre, String valorBusquedaPuesto , String valorBusquedaRequisito);

    @Modifying
    @Query(value = "select \n" +
            "latitud,\n" +
            "longitud\n" +
            "from empresa", nativeQuery = true)
    List<Object[]> findUbicaciones();


}
