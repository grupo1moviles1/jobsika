package com.backend.jobsika.repository;

import com.backend.jobsika.entity.DetalleUsuarioRol;
import com.backend.jobsika.entity.DetalleUsuarioRolPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface DetalleUsuarioRepository extends JpaRepository<DetalleUsuarioRol, DetalleUsuarioRolPK> {
    Optional<List<DetalleUsuarioRol>> findByDetalleUsuarioRolPKIdUsuario(UUID idUsuario);
}
