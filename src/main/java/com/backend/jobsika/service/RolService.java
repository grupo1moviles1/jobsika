package com.backend.jobsika.service;

import com.backend.jobsika.entity.Rol;

import java.util.List;
import java.util.Optional;
import java.util.UUID;


public interface RolService {
    Optional<List<Rol>> findAllIdRol(List<UUID> idRol);
    Optional<Rol> findByNombreRol(String nombreRol);
}
