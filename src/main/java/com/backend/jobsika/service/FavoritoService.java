package com.backend.jobsika.service;

import com.backend.jobsika.dto.PuestoEmpresaDto;
import com.backend.jobsika.exception.CustomException;

import java.util.List;
import java.util.UUID;

public interface FavoritoService {
    List<PuestoEmpresaDto> findAllFavoritos(String idusuario);
    void registrar(UUID idusuario, UUID idPuesto) throws CustomException;
    void eliminar(UUID idusuario, UUID idPuesto) throws CustomException;
}
