package com.backend.jobsika.service;

import com.backend.jobsika.dto.CoordenadasDto;
import com.backend.jobsika.dto.PuestoEmpresaDto;

import java.util.List;
import java.util.UUID;

public interface PuestoService {
    List<PuestoEmpresaDto> findAllPuestoEmpresa(UUID idUsuario);
    List<PuestoEmpresaDto> findAllPuestoEmpresaBusqueda(UUID idUsuario, String valorBusqueda);
    List<CoordenadasDto> findAllubicaciones();
}
