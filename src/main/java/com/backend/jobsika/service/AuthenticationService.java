package com.backend.jobsika.service;


import com.backend.jobsika.exception.CustomException;

public interface AuthenticationService {

    Object authenticate(String usuario,String clave) throws Exception;
    void recuperarClaveGenerarCodigoValidacion(String correo) throws Exception;

    Object authenticateNuevaClave(String usuario,String clave) throws Exception;
}
