package com.backend.jobsika.service;

import com.backend.jobsika.dto.PuestoEmpresaDto;
import com.backend.jobsika.exception.CustomException;

import java.util.List;
import java.util.UUID;

public interface PostulacionService {
    List<PuestoEmpresaDto> findPostulacionByIdUsuario(UUID idUsuario);
    void registrarPostulacion(UUID idUsuario, UUID idEmpresa, UUID idPuesto) throws CustomException;

    void eliminarPostulacion(UUID idUsuario, UUID idEmpresa, UUID idPuesto) throws CustomException;
}
