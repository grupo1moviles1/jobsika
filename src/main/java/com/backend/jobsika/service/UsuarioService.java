package com.backend.jobsika.service;

import com.backend.jobsika.dto.UsuarioDto;
import com.backend.jobsika.entity.Usuario;
import com.backend.jobsika.exception.CustomException;
import org.springframework.security.core.userdetails.User;

import java.util.Optional;

public interface UsuarioService {
    Optional<User> findByNombreUsuario(String correo) throws Exception;
    Optional<Usuario> findByCorreo(String correo) throws Exception;
    Usuario registrarUsuario(UsuarioDto usuarioDto) throws Exception;
    Usuario actualizarUsuario(UsuarioDto usuarioDto) throws Exception;

    Optional<Usuario> crearClaveSeguridad(String correo) throws Exception;
    Optional<Usuario> crearNuevClave(String correo, String nuevaClave) throws Exception;

    void validarCodigoSeguridad(String codigoseguridad,String correo) throws Exception;


}
