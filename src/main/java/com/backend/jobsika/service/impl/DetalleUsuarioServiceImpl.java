package com.backend.jobsika.service.impl;

import com.backend.jobsika.entity.DetalleUsuarioRol;
import com.backend.jobsika.repository.DetalleUsuarioRepository;
import com.backend.jobsika.service.DetalleUsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class DetalleUsuarioServiceImpl implements DetalleUsuarioService {

    @Autowired
    private DetalleUsuarioRepository detalleUsuarioRepository;

    @Override
    public Optional<List<DetalleUsuarioRol>> findByIdUsuario(UUID idUsuario) {
        return detalleUsuarioRepository.findByDetalleUsuarioRolPKIdUsuario(idUsuario);
    }

    @Override
    public Optional<DetalleUsuarioRol> registrarDetalleUsuarioRol(DetalleUsuarioRol detalleUsuarioRol) {
        return Optional.of(detalleUsuarioRepository.save(detalleUsuarioRol));
    }
}
