package com.backend.jobsika.service.impl;

import com.backend.jobsika.entity.Rol;
import com.backend.jobsika.repository.RolRepository;
import com.backend.jobsika.service.RolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class RolServiceImpl implements RolService {

    @Autowired
    private RolRepository rolRepository;

    @Override
    public Optional<List<Rol>> findAllIdRol(List<UUID> idRol) {

        return rolRepository.findAllIdRol(idRol);
    }

    @Override
    public Optional<Rol> findByNombreRol(String nombreRol) {

        return rolRepository.findByNombreRol(nombreRol);
    }
}
