package com.backend.jobsika.service.impl;

import com.backend.jobsika.dto.CoordenadasDto;
import com.backend.jobsika.dto.PuestoEmpresaDto;
import com.backend.jobsika.repository.PuestoRepository;
import com.backend.jobsika.service.PuestoService;
import org.hibernate.query.NativeQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.util.*;

@Service
public class PuestoServiceImpl implements PuestoService {

    @Autowired
    private PuestoRepository puestoRepository;

    /*@PersistenceContext
    private EntityManager entityManager;*/


    @Override
    public List<PuestoEmpresaDto> findAllPuestoEmpresa(UUID idUsuario) {

        List<PuestoEmpresaDto> listresult = new ArrayList<>(0);

        try{


            for (Object[] data : puestoRepository.buscarPuestosEmpresas(idUsuario, idUsuario)){

                PuestoEmpresaDto p = new PuestoEmpresaDto();
                p.setIdPuesto(UUID.fromString(data[0].toString()));
                p.setIdEmpresa(UUID.fromString(data[1].toString()));
                p.setNombrePuesto(Objects.toString(data[2], null));
                p.setDescripcionPuesto(Objects.toString(data[3], null));
                p.setRequisitosPuesto(Objects.toString(data[4], null));
                p.setSueldo(new BigDecimal(Objects.toString(data[5], "0")));
                p.setNombreEmpresa(Objects.toString(data[6], null));
                p.setLogoEmpresa(Objects.toString(data[7], null));
                p.setLongitud(Objects.toString(data[8], null));
                p.setLatitud(Objects.toString(data[9], null));
                p.setFavorito(Objects.toString(data[10], "0").equals("1") ? true : false);
                p.setPostulado(Objects.toString(data[11], "0").equals("1") ? true : false);
                listresult.add(p);

            }

        }catch (Exception e){
            e.printStackTrace();
        }

        return listresult;
    }

    @Override
    public List<PuestoEmpresaDto> findAllPuestoEmpresaBusqueda(UUID idUsuario, String valorBusqueda) {

        List<PuestoEmpresaDto> listresult = new ArrayList<>(0);

        try{


            for (Object[] data : puestoRepository.buscarPuestosEmpresasBusqueda(idUsuario, idUsuario, valorBusqueda, valorBusqueda, valorBusqueda)){

                PuestoEmpresaDto p = new PuestoEmpresaDto();
                p.setIdPuesto(UUID.fromString(data[0].toString()));
                p.setIdEmpresa(UUID.fromString(data[1].toString()));
                p.setNombrePuesto(Objects.toString(data[2], null));
                p.setDescripcionPuesto(Objects.toString(data[3], null));
                p.setRequisitosPuesto(Objects.toString(data[4], null));
                p.setSueldo(new BigDecimal(Objects.toString(data[5], "0")));
                p.setNombreEmpresa(Objects.toString(data[6], null));
                p.setLogoEmpresa(Objects.toString(data[7], null));
                p.setLongitud(Objects.toString(data[8], null));
                p.setLatitud(Objects.toString(data[9], null));
                p.setFavorito(Objects.toString(data[10], "0").equals("1") ? true : false);
                p.setPostulado(Objects.toString(data[11], "0").equals("1") ? true : false);
                listresult.add(p);

            }

        }catch (Exception e){
            e.printStackTrace();
        }

        return listresult;

    }

    @Override
    public List<CoordenadasDto> findAllubicaciones() {

        List<CoordenadasDto> listUbicaciones = new ArrayList<>(0);

        for (Object[] data : puestoRepository.findUbicaciones()){
            listUbicaciones.add(new CoordenadasDto(Objects.toString(data[0], null),Objects.toString(data[1], null)) );
        }
        return listUbicaciones;
    }
}
