package com.backend.jobsika.service.impl;

import com.backend.jobsika.dto.PuestoEmpresaDto;
import com.backend.jobsika.entity.Postulacion;
import com.backend.jobsika.entity.PostulacionPK;
import com.backend.jobsika.exception.CustomException;
import com.backend.jobsika.repository.PostulacionRepository;
import com.backend.jobsika.service.PostulacionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

@Service
public class PostulacionServiceImpl implements PostulacionService {

    @Autowired
    private PostulacionRepository postulacionRepository;

    @Override
    public List<PuestoEmpresaDto> findPostulacionByIdUsuario(UUID idUsuario) {

        List<PuestoEmpresaDto> listresult = new ArrayList<>(0);

        try{
            for (Object[] data : postulacionRepository.findPostulacionByIdUsuario(idUsuario)){
                PuestoEmpresaDto p = new PuestoEmpresaDto();
                p.setIdPuesto(UUID.fromString(data[0].toString()));
                p.setIdEmpresa(UUID.fromString(data[1].toString()));
                p.setNombrePuesto(Objects.toString(data[2], null));
                p.setDescripcionPuesto(Objects.toString(data[3], null));
                p.setRequisitosPuesto(Objects.toString(data[4], null));
                p.setSueldo(new BigDecimal(Objects.toString(data[5], "0")));
                p.setNombreEmpresa(Objects.toString(data[6], null));
                p.setLogoEmpresa(Objects.toString(data[7], null));
                p.setLongitud(Objects.toString(data[8], null));
                p.setLatitud(Objects.toString(data[9], null));
                p.setFavorito(Objects.toString(data[10], "0").equals("1") ? true : false);
                p.setPostulado(Objects.toString(data[11], "0").equals("1") ? true : false);
                listresult.add(p);
            }

        }catch(Exception e){
            e.printStackTrace();

        }

        return listresult;

    }

    @Override
    public void registrarPostulacion(UUID idUsuario, UUID idEmpresa, UUID idPuesto) throws CustomException {

        Postulacion existe = postulacionRepository.findPostulacion(idUsuario, idEmpresa, idPuesto);
        if( existe != null){

            if(existe.getEstado().equals("0")){
                existe.setEstado("1");
                postulacionRepository.save(existe);
                return;
            }else {
                throw new CustomException(HttpStatus.UNPROCESSABLE_ENTITY, "Ya se registro anteriormente!");
            }
        }

        PostulacionPK postPk = new PostulacionPK();
        postPk.setIdEmpresa(idEmpresa);
        postPk.setIdPuesto(idPuesto);
        postPk.setIdUsuario(idUsuario);

        Postulacion postulacion = new Postulacion();
        postulacion.setPostulacionPK(postPk);
        postulacion.setEstadoPostulacion("0");


        postulacion.setFechaCreacion(new Date());
        postulacion.setUsuarioCreado(idUsuario);
        postulacion.setEstado("1");

        postulacionRepository.save(postulacion);

    }

    @Override
    public void eliminarPostulacion(UUID idUsuario, UUID idEmpresa, UUID idPuesto) throws CustomException {
        Postulacion existe = postulacionRepository.findPostulacion(idUsuario, idEmpresa, idPuesto);
        existe.setEstado("0");
        postulacionRepository.save(existe);
    }

}
