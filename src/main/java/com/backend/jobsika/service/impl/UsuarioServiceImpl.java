package com.backend.jobsika.service.impl;

import com.backend.jobsika.dto.UsuarioDto;
import com.backend.jobsika.entity.DetalleUsuarioRol;
import com.backend.jobsika.entity.DetalleUsuarioRolPK;
import com.backend.jobsika.entity.Rol;
import com.backend.jobsika.entity.Usuario;
import com.backend.jobsika.exception.CustomException;
import com.backend.jobsika.repository.UsuarioRepository;
import com.backend.jobsika.service.DetalleUsuarioService;
import com.backend.jobsika.service.RolService;
import com.backend.jobsika.service.UsuarioService;
import com.backend.jobsika.util.Constantes;
import com.backend.jobsika.util.Utiles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class UsuarioServiceImpl implements UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private DetalleUsuarioService detalleUsuarioService;

    @Autowired
    private RolService rolService;

    /*@Autowired
    private AuthenticationService authenticationService;*/


    @Lazy
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public Optional<User> findByNombreUsuario(String correo) throws Exception {
        Optional<Usuario> data = usuarioRepository.findByCorreo(correo);

        if(!data.isPresent()){
            throw new CustomException(HttpStatus.UNPROCESSABLE_ENTITY, "Usuario no valido!");
        }

        Optional<List<DetalleUsuarioRol>> listDetalleUsuarioRoles = detalleUsuarioService.findByIdUsuario(data.get().getIdUsuario());

        List<UUID> listIdRol = listDetalleUsuarioRoles.get().stream().map(detalleUsuarioRol -> detalleUsuarioRol.getDetalleUsuarioRolPK().getIdRol()).collect(Collectors.toList());

        Optional<List<Rol>> roles = rolService.findAllIdRol(listIdRol);

        Collection<SimpleGrantedAuthority> rolesPerfil = roles.get().stream().map(rols -> new SimpleGrantedAuthority(rols.getNombreRol())).collect(Collectors.toList());

        User user = new User(data.get().getCorreo(), data.get().getClave(), rolesPerfil);

        return Optional.of(user);
    }

    @Override
    public Optional<Usuario> findByCorreo(String correo) throws Exception {
        Optional<Usuario> us = usuarioRepository.findByCorreo(correo);
        if(!us.isPresent()){
            throw new CustomException(HttpStatus.UNPROCESSABLE_ENTITY, "El usuario no existe!");
        }
        return us;
    }

    @Override
    @Transactional
    public Usuario registrarUsuario(UsuarioDto usuarioDto) throws Exception {

        Optional<Usuario> data = usuarioRepository.findByCorreo(usuarioDto.getCorreo());

        if (data.isPresent()) {
            throw new CustomException(HttpStatus.UNPROCESSABLE_ENTITY, "Usuario ya existente!");
        }

        String clave = usuarioDto.getClave();
        UUID uuid = UUID.randomUUID();
        Date fechaHoy = new Date();

        usuarioDto.setClave(passwordEncoder.encode(usuarioDto.getClave()));

        Usuario usuarioInsertar = Utiles.usuarioDtoToUsuario(usuarioDto);

        usuarioInsertar.setIndicadorEmpresa(Constantes.ESTADO_SIN_EMPRESA);
        usuarioInsertar.setUsuarioCreado(uuid);
        usuarioInsertar.setFechaCreacion(fechaHoy);
        usuarioInsertar.setEstado(Constantes.ESTADO_REGISTRO_ACTIVO);

        Usuario rs = usuarioRepository.save(usuarioInsertar);

        Optional<Rol> rolDefault = rolService.findByNombreRol(Constantes.NOMBRE_DEFAULT_ROL);
        //Optional<Rol> rolDefaultAdmin = rolService.findByNombreRol(Constantes.NOMBRE_DEFAULT_ROL_ADMIN);

        DetalleUsuarioRol detalleUsuarioRol = new DetalleUsuarioRol();
        detalleUsuarioRol.setDetalleUsuarioRolPK(new DetalleUsuarioRolPK());
        detalleUsuarioRol.getDetalleUsuarioRolPK().setIdUsuario(rs.getIdUsuario());
        detalleUsuarioRol.getDetalleUsuarioRolPK().setIdRol(rolDefault.get().getIdRol());
        detalleUsuarioRol.setUsuarioCreado(rs.getIdUsuario());
        detalleUsuarioRol.setFechaCreacion(fechaHoy);
        detalleUsuarioRol.setEstado(Constantes.ESTADO_REGISTRO_ACTIVO);

        Optional<DetalleUsuarioRol> rsDetalleUserRole = detalleUsuarioService.registrarDetalleUsuarioRol(detalleUsuarioRol);

        return usuarioInsertar;
        //return authenticationService.authenticate(rs.getCorreo(), clave);
    }

    @Override
    public Usuario actualizarUsuario(UsuarioDto usuarioDto) throws Exception {

        Optional<Usuario> us = usuarioRepository.findByCorreo(usuarioDto.getCorreo());

        if(us.isPresent()){

            us.get().setNombreUsuario(usuarioDto.getNombreUsuario());
            us.get().setApellidoUsuario(usuarioDto.getApellidoUsuario());
            us.get().setCorreo(usuarioDto.getCorreo());
            us.get().setDni(usuarioDto.getDni());
            us.get().setFechaNacimiento(usuarioDto.getFechaNacimiento());
            us.get().setSexo(usuarioDto.getSexo());
            us.get().setTelefono(usuarioDto.getTelefono());
            us.get().setFoto(usuarioDto.getFoto());

            usuarioRepository.save(us.get());

        }else{
            throw new CustomException(HttpStatus.UNPROCESSABLE_ENTITY, "Usuario no existe!");
        }

        return null;
    }

    @Override
    public Optional<Usuario> crearClaveSeguridad(String correo) throws Exception {


        Optional<Usuario> usuario = this.findByCorreo(correo);

        if(!usuario.isPresent()){
            throw new CustomException(HttpStatus.UNPROCESSABLE_ENTITY, "El usuario no existe!");
        }

        int cuatroDigitosRandom = (int) (1000 + Math.random() * 9000);

        usuario.get().setCodigoRecuperar(String.valueOf(cuatroDigitosRandom));
        usuarioRepository.save(usuario.get());

        return usuario;
    }

    @Override
    public Optional<Usuario> crearNuevClave(String correo, String nuevaClave) throws Exception {

        Optional<Usuario> usuario = findByCorreo(correo);

        if(!usuario.isPresent()){
            throw new CustomException(HttpStatus.UNPROCESSABLE_ENTITY, "El usuario no existe!");
        }

        usuario.get().setClave(passwordEncoder.encode(nuevaClave));
        usuarioRepository.save(usuario.get());

        return usuario;

    }

    @Override
    public void validarCodigoSeguridad(String codigoseguridad, String correo) throws Exception {
        Optional<Usuario> usuario = findByCorreo(correo);

        if(!usuario.isPresent()){
            throw new CustomException(HttpStatus.UNPROCESSABLE_ENTITY, "El usuario no existe!");
        }
        if(!usuario.get().getCodigoRecuperar().equals(codigoseguridad)){
            throw new CustomException(HttpStatus.UNPROCESSABLE_ENTITY, "El código no es valido!");
        }
    }
}
