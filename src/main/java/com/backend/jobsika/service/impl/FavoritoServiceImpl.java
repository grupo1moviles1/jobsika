package com.backend.jobsika.service.impl;

import com.backend.jobsika.dto.PuestoEmpresaDto;
import com.backend.jobsika.entity.Favorito;
import com.backend.jobsika.entity.FavoritoPK;
import com.backend.jobsika.exception.CustomException;
import com.backend.jobsika.repository.FavoritoRepository;
import com.backend.jobsika.service.FavoritoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

@Service
public class FavoritoServiceImpl implements FavoritoService {

    @Autowired
    private FavoritoRepository favoritoRepository;

    @Override
    public List<PuestoEmpresaDto> findAllFavoritos(String idusuario) {

        List<PuestoEmpresaDto> listresult = new ArrayList<>(0);

        try{
            for (Object[] data : favoritoRepository.findAllFavoritos(UUID.fromString(idusuario), UUID.fromString(idusuario))){
                PuestoEmpresaDto p = new PuestoEmpresaDto();
                p.setIdPuesto(UUID.fromString(data[0].toString()));
                p.setIdEmpresa(UUID.fromString(data[1].toString()));
                p.setNombrePuesto(Objects.toString(data[2], null));
                p.setDescripcionPuesto(Objects.toString(data[3], null));
                p.setRequisitosPuesto(Objects.toString(data[4], null));
                p.setSueldo(new BigDecimal(Objects.toString(data[5], "0")));
                p.setNombreEmpresa(Objects.toString(data[6], null));
                p.setLogoEmpresa(Objects.toString(data[7], null));
                p.setLongitud(Objects.toString(data[8], null));
                p.setLatitud(Objects.toString(data[9], null));
                p.setFavorito(Objects.toString(data[10], "0").equals("1") ? true : false);
                p.setPostulado(Objects.toString(data[11], "0").equals("1") ? true : false);
                listresult.add(p);
            }

        }catch(Exception e){
            e.printStackTrace();

        }

        return listresult;
    }

    @Override
    public void registrar(UUID idusuario, UUID idPuesto) throws CustomException {

        Favorito fa = favoritoRepository.findFavorito(idusuario,idPuesto );

        if(fa != null){

            if(fa.getEstado().equals("0")){
                fa.setEstado("1");
                favoritoRepository.save(fa);
                return;
            }else{
                throw new CustomException(HttpStatus.UNPROCESSABLE_ENTITY, "Ya se registro anteriormente!");
            }
        }

        fa = new Favorito();

        FavoritoPK fpk = new FavoritoPK();
        fpk.setIdPuesto(idPuesto);
        fpk.setIdUsuario(idusuario);

        fa.setFavoritoPK(fpk);
        fa.setEstado("1");
        fa.setFechaCreacion(new Date());
        fa.setUsuarioCreado(idusuario);

        favoritoRepository.save(fa);

    }

    @Override
    public void eliminar(UUID idusuario, UUID idPuesto) throws CustomException {
        Favorito fa = favoritoRepository.findFavorito(idusuario,idPuesto );
        fa.setEstado("0");
        favoritoRepository.save(fa);
    }
}
