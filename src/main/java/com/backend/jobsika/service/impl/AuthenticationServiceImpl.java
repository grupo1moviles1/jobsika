package com.backend.jobsika.service.impl;

import com.backend.jobsika.config.JwtService;
import com.backend.jobsika.entity.Usuario;
import com.backend.jobsika.exception.CustomException;
import com.backend.jobsika.service.AuthenticationService;
import com.backend.jobsika.service.UsuarioService;
import net.bytebuddy.implementation.bytecode.Throw;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private JwtService jwtService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Override
    public Object authenticate(String usuario, String clave) throws Exception {


        Optional<User> userData = usuarioService.findByNombreUsuario(usuario);

        if(!userData.isPresent()){
            throw new CustomException(HttpStatus.UNPROCESSABLE_ENTITY, "Usuario no valido!");
        }

        try{
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(usuario, clave));

            String jwtToken = jwtService.generateToken(userData.get());

            return jwtToken;

        }catch (BadCredentialsException be){
            throw new CustomException(HttpStatus.UNPROCESSABLE_ENTITY, "Usuario / clave no valido!");
        }catch (Exception e){
            e.printStackTrace();
        }

        return null;

    }

    @Override
    public void recuperarClaveGenerarCodigoValidacion(String correo) throws Exception {

        usuarioService.crearClaveSeguridad(correo);

    }

    @Override
    public Object authenticateNuevaClave(String usuario, String clave) throws Exception {
        String claveNueva = clave;
        Optional<Usuario> usuarioNuevo = usuarioService.crearNuevClave(usuario, clave);
        return authenticate(usuario, claveNueva);
    }
}
