package com.backend.jobsika.service;

import com.backend.jobsika.entity.DetalleUsuarioRol;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface DetalleUsuarioService {
    Optional<List<DetalleUsuarioRol>> findByIdUsuario(UUID idUsuario);
    Optional<DetalleUsuarioRol> registrarDetalleUsuarioRol(DetalleUsuarioRol detalleUsuarioRol);
}
